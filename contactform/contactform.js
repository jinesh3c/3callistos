jQuery(document).ready(function($) {
  "use strict";

  //Contact
  $('form.contactForm').submit(function(e) {
    var f = $(this).find('.form-group'),
      ferror = false,
      emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;

    f.children('input').each(function() { // run all inputs

      var i = $(this); // current input
      var rule = i.attr('data-rule');

      if (rule !== undefined) {
        var ierror = false; // error flag for current input
        var pos = rule.indexOf(':', 0);
        if (pos >= 0) {
          var exp = rule.substr(pos + 1, rule.length);
          rule = rule.substr(0, pos);
        } else {
          rule = rule.substr(pos + 1, rule.length);
        }

        switch (rule) {
          case 'required':
            if (i.val() === '') {
              ferror = ierror = true;
            }
            break;

          case 'minlen':
            if (i.val().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;

          case 'maxlen':
            if (i.val().length > parseInt(exp)) {
              ferror = ierror = true;
            }
            break;

          case 'email':
            if (!emailExp.test(i.val())) {
              ferror = ierror = true;
            }
            break;

          case 'checked':
            if (! i.is(':checked')) {
              ferror = ierror = true;
            }
            break;

          case 'regexp':
            exp = new RegExp(exp);
            if (!exp.test(i.val())) {
              ferror = ierror = true;
            }
            break;
        }
        i.next('.validation').html((ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
      }
    });
    f.children('textarea').each(function() { // run all inputs

      var i = $(this); // current input
      var rule = i.attr('data-rule');

      if (rule !== undefined) {
        var ierror = false; // error flag for current input
        var pos = rule.indexOf(':', 0);
        if (pos >= 0) {
          var exp = rule.substr(pos + 1, rule.length);
          rule = rule.substr(0, pos);
        } else {
          rule = rule.substr(pos + 1, rule.length);
        }

        switch (rule) {
          case 'required':
            if (i.val() === '') {
              ferror = ierror = true;
            }
            break;

          case 'minlen':
            if (i.val().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;
        }
        i.next('.validation').html((ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
      }
    });
    if (ferror) return false;
    // else var str = $(this).serialize();
    else{
        $.ajax({
            url:'contact.php', 
            data:{
                name: $('#name').val(),
                phone: $('#phone').val(),
                email: $('#email').val(),
                subject: $('#subject').val(),
                message: $('#message').val(),
               },
            beforeSend: function(){
            // Show image container
              $("#loader").show();
              $('#ajaxButton').hide();
             },
            type:'POST',
            success:function(response){
              console.log(response);
              if(response=="true"){
                var database = firebase.database();
                    firebase.database().ref('contacts').push({
                      name: document.getElementById("name").value,
                      email: document.getElementById("email").value,
                      subject: document.getElementById("subject").value,
                      phone: document.getElementById("phone").value,
                      message: document.getElementById("message").value,
                    });
                swal("your message has been sent","we reply you soon","success")
              }
              if(response=="false"){
                swal("Error", "Failed to send mail", "error");
              }
            },
            complete:function(data){
            // Hide image container
              $("#loader").hide();
              $('#ajaxButton').show();
             }
          });
          e.preventDefault();
    }
    return false;
  });

});
